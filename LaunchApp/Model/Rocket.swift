//
//  Rocket.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation

struct Rocket : Decodable{
    let id: Int
    let name: String
    let configuration: String
    let imageURL: String
}

