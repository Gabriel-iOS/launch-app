////
////  LaunchList.swift
////  LaunchApp
////
////  Created by Gabriel on 04/12/2018.
////  Copyright © 2018 Gabriel. All rights reserved.
////
//
//import Foundation

struct LaunchList: Decodable{
    var launches : [Launch]
    
    
    func getArrayLaunchId() -> [Int]{
        
        var launchIds = [Int]()
        
        for launch in self.launches {
            launchIds.append(launch.id)
        }
        
        return launchIds
    }
    
    
    mutating func setFavoriteForLaunchId(launchTmp : Launch, favorite: Bool){
        
        var launchesTmp = [Launch]()
        
        for launch in self.launches{
            var launchTmmp = launch
            
            if launch.id == launchTmp.id{
                launchTmmp.favorited = favorite
            }
            
            launchesTmp.append(launchTmmp)
        }
        
        self.launches = launchesTmp
    }
}
