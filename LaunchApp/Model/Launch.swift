//
//  Launch.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation

struct Launch: Decodable {
    let id: Int
    let name, windowstart, windowend : String
    
    let status: Int
    
    let location: Location
    let rocket: Rocket
    let missions: [Mission]
    let agency: Agency
    
    var favorited = false
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case windowstart
        case windowend
        case status
        case location
        case rocket
        case missions
        case agency = "lsp"
    }
    
    
    
    func getRocketDetails() -> String{
        return self.rocket.name
    }
    
    func getLocationDetails() -> String{
        return "\(self.location.name) | \(self.location.countryCode)"
    }
    
    func getAgencyDetails() -> String{
        var agencyDetails = ""
        
        for mission in self.missions{
            
            if let agencies =  mission.agencies{
                for agency in mission.agencies!{
                    agencyDetails = agencyDetails + "\(agency.name) | \(agency.countryCode),"
                }
            }
        }
        
        if(agencyDetails.count > 0){
            agencyDetails.removeLast()
        }
        
        return agencyDetails
    }
}






