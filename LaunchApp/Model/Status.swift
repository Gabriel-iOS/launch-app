//
//  Status.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation

struct StatusList : Decodable{
    let status : [Status]
    
    enum CodingKeys : String, CodingKey {
        case status = "types"
    }
}

struct Status : Decodable{
    let id: Int
    let name: String
}
