//
//  LaunchDTO.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class LaunchDTO: NSObject {
    
    let urlWebserviceList = "https://launchlibrary.net/1.4/launch/next/50"
    
    var launchList : LaunchList?
    
    
    func refreshLaunchArray(){
        
        launchList = nil
        
        Webservices.shared.sendRequest(urlWebserviceList, parameters: [:]) { (value, error) in
            
            guard let value = value else {
                print("Error while fetching tags: \(String(describing: error))")
                return
            }
            
            do{
                    let newJSONDecoder = JSONDecoder()
                    let itemList = try newJSONDecoder.decode(LaunchList.self, from: value)
                    self.launchList = itemList
                    
                    //Notification controller
                    let notifName = Notification.Name(rawValue: "ItemsLoaded")
                    let notification = Notification(name: notifName)
                    NotificationCenter.default.post(notification)
                    print("END REFRESH items list")
                }catch{
                    print(error)
                }
        }
    }
}
