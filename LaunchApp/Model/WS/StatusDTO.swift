//
//  StatusDTO.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON

class StatusDTO: NSObject {
    
    let urlWebserviceList = "https://launchlibrary.net/1.4/launchstatus"
    
    static let shared = StatusDTO()
    var launchStatusList : StatusList?
    
    
    func refreshLaunchStatusArray(){
        
        launchStatusList = nil
        
        Webservices.shared.sendRequest(urlWebserviceList, parameters: [:]) { (value, error) in
            
            guard let value = value else {
                print("Error while fetching tags: \(String(describing: error))")
                return
            }
            
            do{
                let newJSONDecoder = JSONDecoder()
                let itemList = try newJSONDecoder.decode(StatusList.self, from: value)
                self.launchStatusList = itemList
            }catch{
                print(error)
            }
        }
    }
    
    
    func getStatusById(id : Int) -> String{
        
        for status in self.launchStatusList!.status{
            if status.id == id{
                return status.name
            }
        }
        return "No provided"
    }
}

