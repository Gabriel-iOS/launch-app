//
//  MasterTableViewController.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import UIKit
import SDWebImage

class MasterTableViewController: UITableViewController, UISplitViewControllerDelegate{
    
    var launchDTO = LaunchDTO()
    weak var delegate: ItemSelectionDelegate?
   // var detailViewController : DetailsViewController?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "ItemCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ItemCell")
        
        splitViewController?.delegate = self
        
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.5;
        splitViewController?.maximumPrimaryColumnWidth = (splitViewController?.view.bounds.size.width)!;

        // GET ITEMS LIST
        let notificationName = Notification.Name(rawValue: "ItemsLoaded")
        NotificationCenter.default.addObserver(self, selector: #selector(itemsLoaded), name: notificationName, object: nil)
        
        self.delegate = splitViewController?.viewControllers.last as? DetailsViewController
        
        StatusDTO.shared.refreshLaunchStatusArray()
        launchDTO.refreshLaunchArray()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        
        self.refreshFavorite()
    }
    
    private func refreshFavorite(){
        let defaults = UserDefaults.standard
        if let favoritedLaunchs = defaults.value(forKey: "FavoritedLaunchs") as? [Int]{
            
            for favorited in favoritedLaunchs{
                if let indexLaunchFavorited = launchDTO.launchList?.getArrayLaunchId().firstIndex(of: favorited){
                    launchDTO.launchList?.launches[indexLaunchFavorited].favorited = true
                }
            }
        }
        
        self.tableView.reloadData()
    }
    
    @objc func itemsLoaded(){
        
        self.refreshFavorite()
        
        if let itemDefault = launchDTO.launchList?.launches.first{
                // Update detail view with item info
                self.delegate?.itemSelected(itemDefault)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = launchDTO.launchList?.launches.count{
            return count
        }
        else{
            return 0
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ItemTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemTableViewCell
        
        if let launchData = launchDTO.launchList?.launches[indexPath.row]{
            cell.configureWith(launch: launchData)
        }
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let selectedItem = self.launchDTO.launchList!.launches[indexPath.row]
        
            self.delegate?.itemSelected(selectedItem)
            if let detailViewController = self.delegate as? DetailsViewController {
                detailViewController.delegate = self
                self.splitViewController?.showDetailViewController(detailViewController, sender: nil)
            }
    }
    
    

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true // Show master first on Portrait mode
    }
}



extension UIImageView{
    // USING SDWebImage
    func setImageFromWeb(urlString : String){
        if let url = URL(string: urlString){
            self.sd_setImage(with: url, placeholderImage: nil, options: .cacheMemoryOnly, progress: { (receivedSize, expectedSize, _) in
                }) { (image, error, _, _) in
            }
        }
    }
}

// Pass item to delegate
protocol ItemSelectionDelegate: class {
    func itemSelected(_ newItem: Launch)
}


extension MasterTableViewController: ItemFavoriteDelegate {
    
    func itemFavorited(_ item: Launch, favorited : Bool) {
        self.launchDTO.launchList?.setFavoriteForLaunchId(launchTmp: item, favorite: favorited)
        self.refreshFavorite()
    }
}
