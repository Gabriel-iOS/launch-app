//
//  DetailsViewController.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import UIKit
import AVKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var AgencyLabel: UILabel!
    
    @IBOutlet weak var agenciesLabel: UILabel!
    @IBOutlet weak var plateformeLabel: UILabel!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    weak var delegate: ItemFavoriteDelegate?
    
    var launchItem : Launch?{
        didSet{
            refreshDetails()
            
            self.favoriteButton.isSelected = self.launchItem!.favorited
        }
    }
    
    private func refreshDetails(){
        loadViewIfNeeded() //Not allowed for device < iOS 9
        self.iconImage.setImageFromWeb(urlString: launchItem!.rocket.imageURL)

        self.nameLabel.text = launchItem?.name
        
        self.detailsLabel.text = launchItem?.getRocketDetails()
        self.plateformeLabel.text = launchItem?.getLocationDetails()
        self.AgencyLabel.text = launchItem?.agency.name
        self.agenciesLabel.text = launchItem?.getAgencyDetails()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.favoriteButton.setBackgroundColor(color: .white, forState: .normal)
        self.favoriteButton.setBackgroundColor(color: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), forState: .selected)
        
        self.favoriteButton.setTitle("Favorite", for: .normal)
        self.favoriteButton.setTitle("Unfavorite", for: .selected)
    }
    
    
    @IBAction func didTapFavoriteButton(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        
        let defaults = UserDefaults.standard
        var favoritedLaunchs = defaults.value(forKey: "FavoritedLaunchs") as? [Int]
        
        if favoritedLaunchs == nil{
            favoritedLaunchs = [Int]()
        }
        
        if sender.isSelected{
            favoritedLaunchs?.append(self.launchItem!.id)
        }
        else{
            if let index = favoritedLaunchs?.firstIndex(of: self.launchItem!.id){
                favoritedLaunchs?.remove(at: index)
            }
        }
        
        defaults.set(favoritedLaunchs, forKey: "FavoritedLaunchs")
        defaults.synchronize()
        
        self.delegate?.itemFavorited(self.launchItem!, favorited: sender.isSelected)
    }
}


extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}

protocol ItemFavoriteDelegate: class {
    func itemFavorited(_ item: Launch, favorited : Bool)
}


extension DetailsViewController: ItemSelectionDelegate {
    
    func itemSelected(_ newItem: Launch) {
        self.launchItem = newItem
    }
}

