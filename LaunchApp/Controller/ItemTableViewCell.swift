//
//  ItemTableViewCell.swift
//  LaunchApp
//
//  Created by Gabriel on 04/12/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var statutLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        var customColorView = UIView()
        customColorView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        
        self.selectedBackgroundView = customColorView
    }
    
    func configureWith(launch : Launch){
        self.nameLabel.text = launch.name
        self.dateLabel.text = launch.windowstart
        self.statutLabel.text = StatusDTO.shared.getStatusById(id:launch.status)
        self.favoriteImage.isHidden = !launch.favorited
        
        let imageUrl = launch.rocket.imageURL
        self.iconImage.setImageFromWeb(urlString: imageUrl)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        var customColorView = UIView()
        if(selected){
            customColorView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            self.nameLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.dateLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.statutLabel.textColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        }
        else{
            customColorView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.dateLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.statutLabel.textColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        }
        
        self.backgroundView = customColorView
        
    }
    
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        var customColorView = UIView()
        if(highlighted){
                customColorView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            self.nameLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        else{
            customColorView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.nameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        
        self.backgroundView = customColorView
        
    }
    
    
    
}
