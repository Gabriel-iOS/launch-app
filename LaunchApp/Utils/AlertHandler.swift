//
//  AlertHandler.swift
//  TestAlamofire
//
//  Created by Gabriel on 15/11/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import UIKit

class AlertHandler: NSObject {
    
    static let shared = AlertHandler()
    fileprivate var currentVC : UIViewController!
    
    var waitUserAction: ((AlertUserSelection) -> Void)?
    
    enum AlertUserSelection {
        case OK, CANCEL, DESTRUCT
    }
    
    
    
// MARK: ONE AND TWO BUTTONS
    
    func showAlertOneButton(title : String, message : String){
        self.currentVC = UIApplication.shared.keyWindow?.rootViewController
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.OK)
        }))
    
        // show the alert
        currentVC.present(alert, animated: true, completion: nil)
    }
    
    func showAlertTwoButton(title : String, message : String){
        self.currentVC = UIApplication.shared.keyWindow?.rootViewController
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.OK)
        }))
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.CANCEL)
        }))
        
        
        // show the alert
        currentVC.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
// MARK: THREE BUTTONS
    
    func showAlertThreeButton(title : String, message : String){
        self.currentVC = UIApplication.shared.keyWindow?.rootViewController
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.OK)
        }))
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.CANCEL)
        }))
        
        alert.addAction(UIAlertAction(title: "DESTRUCT", style: .destructive, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.DESTRUCT)
        }))
        
        
        // show the alert
        currentVC.present(alert, animated: true, completion: nil)
    }
    
    func showAlertThreeButton(title : String, message : String, okButton : String?, cancelButton : String?, destructButton : String?){
        self.currentVC = UIApplication.shared.keyWindow?.rootViewController
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: okButton, style: .default, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.OK)
        }))
        
        alert.addAction(UIAlertAction(title: cancelButton, style: .cancel, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.CANCEL)
        }))
        
        alert.addAction(UIAlertAction(title: destructButton, style: .destructive, handler: { (action : UIAlertAction!) in
            self.waitUserAction?(.DESTRUCT)
        }))
        
        
        // show the alert
        currentVC.present(alert, animated: true, completion: nil)
    }
    
    
    
    
   
// MARK: INTERNET CONNECTIVITY
    
    func showAlertConectivityProlbem(){
        self.currentVC = UIApplication.shared.keyWindow?.rootViewController
        
        // create the alert
        let title = "Connexion perdue"
        let message = "Veuillez vérifiez votre connexion internet"
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
        
        // show the alert
        self.currentVC?.present(alert, animated: true, completion: nil)
    }
}
